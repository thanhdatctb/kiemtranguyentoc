/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai2;

import java.util.Scanner;

/**
 *
 * @author thanh
 */
public class NhaBan extends DiaOc implements Comparable<NhaBan>{
   
    private float GiaBan;

    public float getGiaBan() {
        return GiaBan;
    }

    public void setGiaBan(float GiaBan) {
        this.GiaBan = GiaBan;
    }
    @Override
    public float getTienLai(){
        return this.GiaBan*10/100;
    }

    public NhaBan(String DiaChi, float DienTich,boolean tinhTrang, float GiaBan) {
        this.GiaBan = GiaBan;
        super.setDiaChi(DiaChi);
        super.setDienTich(DienTich);
        super.setTinhTrang(tinhTrang);
    }

    public NhaBan() {
    }
    
    @Override
    public DiaOc NhapThongTin() {
        DiaOc nb = new NhaBan();
        Scanner myObj = new Scanner(System.in);
        System.out.print("Nhập diện tích: ");
        nb.setDienTich(new Scanner(System.in).nextFloat());
        System.out.print("Nhập Địa chỉ: ");
        nb.setDiaChi(new Scanner(System.in).nextLine());
        System.out.print("Nhà đã bán (đã bán: 1, chưa bán: 0 ): ");
        int check = myObj.nextInt();
        if(check == 0){
            nb.setTinhTrang(false);
        }else{
            nb.setTinhTrang(true);
        }
        System.out.print("Nhập giá bán: ");
        float GiaBan = myObj.nextFloat();
        System.out.println("-------------------------------------");
        return new NhaBan(nb.getDiaChi(), nb.getDienTich(), nb.isTinhTrang(), GiaBan);
        
    }

    @Override
    public void XuatThongTin() {
        System.out.println("Diện tich: "+ super.getDienTich());
        System.out.println("Địa chỉ: "+ super.getDiaChi());
        System.out.println("Tình trạng: " + super.isTinhTrang());
        System.out.println("Giá bán: "+this.GiaBan);
        System.out.println("-------------------------------------");
    }

    @Override
    public int compareTo(NhaBan o) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return this.getDiaChi().compareTo(o.getDiaChi());
    }
}
