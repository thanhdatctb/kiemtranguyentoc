/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 *
 * @author thanh
 */
public class Bai2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        List<DiaOc> diaocs = new ArrayList<DiaOc>();
        List<NhaBan> nbs = new ArrayList<NhaBan>();
        List<NhaChoThue> ncts = new ArrayList<NhaChoThue>();
        do {
            System.out.println("1.Nhập, xuất thông tin các loại nhà bán và nhà cho thuê.");
            System.out.println("2.Xuất thông tin các nhà bán.");
            System.out.println("3.Xuất thông tin nhà cho thuê có địa chi là X (nếu có).");
            System.out.println("4. Sắp xếp nhà cho thuê và nhà bán theo địa chi nhà.");
            System.out.println("5. Tính tông tiên lãi mà công ty đã thu được (từ nhà đã bán và nhà đã cho thuê).");
            System.out.println("6. Xóa tất cả các thông tin của nhà đã bán.");
            System.out.print("Nhập lựa chọn: ");
            int option = myObj.nextInt();
            switch (option) {
                case 1:
                    System.out.print("Nhập số lượng nhà bán: ");
                    int numnb = myObj.nextInt();
                    for(int i = 0; i <numnb; i ++){
                        NhaBan nb = (NhaBan) new NhaBan().NhapThongTin();
                        nbs.add(nb);
                    }
                    System.out.print("Nhập số lượng nhà thuê: ");
                    int numnt = myObj.nextInt();
                    for(int i = 0; i <numnt; i ++){
                        NhaChoThue nb = (NhaChoThue) new NhaChoThue().NhapThongTin();
                        ncts.add(nb);
                    }
                    System.out.println("Thông tin nhà bán vừa nhập: ");
                    nbs.stream().forEach(s ->{
                        s.XuatThongTin();
                    });
                    System.out.println("Thông tin nhà cho thuê vừa nhập: ");
                    ncts.stream().forEach(s ->{
                        s.XuatThongTin();
                    });
                    break;
                case 2:
                    System.out.println("Thông tin nhà bán vừa nhập: ");
                    nbs.stream().forEach(s ->{
                        s.XuatThongTin();
                    });
                    break;
                case 3:
                    System.out.print("Nhập địa chỉ cần tìm: ");
                    String dc = new Scanner(System.in).nextLine();
                    List<NhaChoThue> nctX = ncts.stream().filter(s -> s.getDiaChi().equals(dc)).collect(Collectors.toList());
                    nctX.stream().forEach(s ->{
                        s.XuatThongTin();
                    });
                    break;
                case 4:
                    Collections.sort(nbs);
                    Collections.sort(ncts);
                    System.out.println("Thông tin nhà bán đã sắp xếp: ");
                    nbs.stream().forEach(s ->{
                        s.XuatThongTin();
                    });
                    System.out.println("Thông tin nhà cho thuê đã sắp xếp: ");
                    ncts.stream().forEach(s ->{
                        s.XuatThongTin();
                    });
                    break;
                case 5:
                    int total = 0;
                    total += nbs.stream().mapToDouble(s-> s.getTienLai()).sum();
                    total += ncts.stream().mapToDouble(s-> s.getTienLai()).sum();
                    System.out.println("Tổng tiền lãi: "+total);
                    
                    break;
                case 6:
                    nbs.removeIf(s-> s.isTinhTrang()==true);
                    System.out.println("Thông tin nhà cho bán: ");
                    nbs.stream().forEach(s ->{
                        s.XuatThongTin();
                    });
                    break;
                default:
                    System.out.println("Không hợp lệ");
            }
        } while (true);
        // TODO code application logic here

    }

}
