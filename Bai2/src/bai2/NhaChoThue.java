/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai2;

import java.util.Scanner;

/**
 *
 * @author thanh
 */
public class NhaChoThue extends DiaOc implements Comparable<NhaChoThue>{
    private float giaChoThue;
    private int soThangThue;

    public float getGiaChoThue() {
        return giaChoThue;
    }

    public void setGiaChoThue(float giaChoThue) {
        this.giaChoThue = giaChoThue;
    }

    public int getSoThangThue() {
        return soThangThue;
    }
    public float getTongGiaThue(){
        if(this.soThangThue > 24){
            return this.giaChoThue*soThangThue*90/100;
        }
        return this.giaChoThue*soThangThue;
    }
    public void setSoThangThue(int soThangThue) {
        this.soThangThue = soThangThue;
    }
    
    @Override
    public float getTienLai() {
        return this.getTongGiaThue()*10/100;
    }

    public NhaChoThue() {
    }

    public NhaChoThue(String DiaChi, float DienTich,boolean tinhTrang,float giaChoThue, int soThangThue) {
        this.giaChoThue = giaChoThue;
        this.soThangThue = soThangThue;
        super.setDiaChi(DiaChi);
        super.setDienTich(DienTich);
        super.setTinhTrang(tinhTrang);
    }
    
    @Override
    public DiaOc NhapThongTin() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        DiaOc nct = new NhaChoThue();
        Scanner myObj = new Scanner(System.in);
        System.out.print("Nhập diện tích: ");
        nct.setDienTich(new Scanner(System.in).nextFloat());
        System.out.print("Nhập Địa chỉ: ");
        nct.setDiaChi(new Scanner(System.in).nextLine());
        System.out.print("Tình trạng (đã cho thuê: 1, chưa : 0 ): ");
        int check = myObj.nextInt();
        if(check == 0){
            nct.setTinhTrang(false);
        }else{
            nct.setTinhTrang(true);
        }
        System.out.print("Nhập giá cho thuê: ");
        float gct = myObj.nextFloat();
        System.out.print("Số tháng thuê: ");
        int stt = myObj.nextInt();
        System.out.println("-------------------------------------");
        return new NhaChoThue(nct.getDiaChi(), nct.getDienTich(), nct.isTinhTrang(), gct, stt);
    }

    @Override
    public void XuatThongTin() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Diện tich: "+ super.getDienTich());
        System.out.println("Địa chỉ: "+ super.getDiaChi());
        System.out.println("Tình trạng: " + super.isTinhTrang());
        System.out.println("Giá cho thuê: "+this.giaChoThue);
        System.out.println("Số tháng thuê: " + this.soThangThue);
        System.out.println("-------------------------------------");
    }
    

    @Override
    public int compareTo(NhaChoThue o) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//       public int compareTo(NhaBan o) {
//        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return this.getDiaChi().compareTo(o.getDiaChi());
    
    }
}
